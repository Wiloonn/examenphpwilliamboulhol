<?php

require_once 'utils/bdd_connexion.php';
require_once 'utils/function.php';

$idCompetence = $_GET['id'];
$competence = getCompetence($pdo, $idCompetence);
$errors = [];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $returnValidation = validateEditCompetence();
    $errors = $returnValidation['errors'];

    if (count($errors) === 0) {
        updateCompetence($pdo, $competence['id']);
        header('Location: adminpage.php');
    }
}
?>

<form method="post" action="edit-competence.php?id=<?php echo($competence['id']);?>" enctype="multipart/form-data">
    <label>Titre de la compétence</label>
    <input type="text" name="titre" class="form-control" placeholder="Nom de la compétence" value="<?php echo($competence['titre'])?>">
    <label>Note de la compétence</label>
    <input type="number" name="note" class="form-control" placeholder="Note de la compétence" value="<?php echo($competence['note'])?>">

    <input type="submit">

</form>