<?php

// LOGIN

function login($pdo, $login, $password){
    $errors = [];
    try {
        $req = $pdo->prepare(
            'SELECT * FROM user where email = :email and mot_de_passe = :mot_de_passe');

        $res = $req -> execute([
            'email' => $login,
            'mot_de_passe' => md5($password)
        ]);
    } catch (PDOException $exception){
        var_dump($exception);
        die();
    }
    if($req->fetch() === false){
        $errors[] = 'Mauvais identifiants';
        session_destroy();
    } else {
        $_SESSION['user'] = $res;
    }
    return $errors;
}

// CRUD Compétences

// AJOUT
function addCompetence($pdo){
    $req = $pdo->prepare(
        'INSERT INTO competence(titre, note)
    VALUES(:titre, :note)');
    $req->execute([
            'titre' => $_POST['titre'],
            'note' => $_POST['note'],
    ]);
}

function validateFormCompetence(){
    $errors = [];

    if (empty($_POST['titre'])) {
        $errors[] = 'Veuillez saisir le titre de la compétence';
    }
    if ( empty($_POST['note'])) {
        $errors[] = 'Veuillez saisir la note de la compétence';
    }

    return ['errors'=>$errors];
}

// EDIT
function getCompetence($pdo, $id){
    $res = $pdo->prepare('SELECT * FROM competence WHERE id = :id');
    $res->execute(['id'=> $id]);
    return $res->fetch();
}

function validateEditCompetence(){
    $errors = [];

    if (empty($_POST['titre'])){
        $errors[] = 'Veuillez saisir le titre de la compétence';
    }
    if (empty($_POST['note'])){
        $errors[] = 'Veuillez saisir la note de la compétence';
    }
}

function updateCompetence($pdo, $id) {
    $req = $pdo->prepare('UPDATE competence set titre = :titre, note = :note WHERE id = :id');
    $req->execute([
        'titre' => $_POST['titre'],
        'note' => $_POST['note'],
        'id' => $id
    ]);
}

// DELETE
function deleteCompetence($pdo, $id)
{
    $res = $pdo->prepare('DELETE FROM competence WHERE id = :id');
    $res->execute(['id'=> $id]);
}

// CRUD EXPERIENCES

// AJOUT
function addExp($pdo){
    $req = $pdo->prepare(
        'INSERT INTO experience(titre, description)
    VALUES(:titre, :desciption, :date_debut, :date_fin)');
    $req->execute([
        'titre' => $_POST['titre'],
        'description' => $_POST['description'],
        'date_debut' => $_POST['date_debut'],
        'date_fin' => $_POST['date_fin']
    ]);
}

function validateFormExp(){
    $errors = [];

    if (empty($_POST['titre'])) {
        $errors[] = 'Veuillez saisir le titre de l\'expérience';
    }
    if ( empty($_POST['description'])) {
        $errors[] = 'Veuillez saisir la description de l\'experience';
    }
    if (empty($_POST['date_debut'])) {
        $errors[] = 'Veuillez saisir la date de début de votre experience';
    }

    return ['errors'=>$errors];
}

// EDIT
function getExp($pdo, $id){
    $res = $pdo->prepare('SELECT * FROM experience WHERE id = :id');
    $res->execute(['id'=> $id]);
    return $res->fetch();
}

function validateEditExp(){
    $errors = [];

    if (empty($_POST['titre'])){
        $errors[] = 'Veuillez saisir le titre de l\'expérience';
    }
    if (empty($_POST['description'])){
        $errors[] = 'Veuillez saisir la description de l\'experience';
    }
    if (empty($_POST['date_debut'])) {
        $errors[] = 'Veuillez saisir la date de debut de l\'expérience';
    }
}

function updateExp($pdo, $id) {
    $req = $pdo->prepare('UPDATE experience set titre = :titre, description = :description, date_debut = :date_debut, date_fin = :date_fin WHERE id = :id');
    $req->execute([
        'titre' => $_POST['titre'],
        'description' => $_POST['description'],
        'date_debut' => $_POST['date_debut'],
        'date_fin' => $_POST['date_fin'],
        'id' => $id
    ]);
}

// DELETE
function deleteExp($pdo, $id)
{
    $res = $pdo->prepare('DELETE FROM experience WHERE id = :id');
    $res->execute(['id'=> $id]);
}
