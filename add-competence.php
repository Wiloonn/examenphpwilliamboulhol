<?php
require_once 'utils/bdd_connexion.php';
require_once  'utils/function.php';

$errors = [];
if ( $_SERVER['REQUEST_METHOD'] === 'POST') {
    $returnValidation = validateFormCompetence();
    $errors = $returnValidation['errors'];
    if (count($errors) === 0) {
        addCompetence($pdo);
        header('Location: adminpage.php');
    }
}
?>

<form method="post" action="add-competence.php" enctype="multipart/form-data">
    <label>Titre de la compétence</label>
    <input type="text" name="titre" class="form-control" placeholder="Nom de la compétence">
    <label>Note de la compétence</label>
    <input type="number" name="note" class="form-control" placeholder="Note de la compétence">

    <input type="submit">

    <?php
    if(count($errors) != 0){
        echo(' <h2>Erreurs lors de la dernière soumission du formulaire : </h2>');
        foreach ($errors as $error){
            echo('<div class="error">'.$error.'</div>');
        }
    }
    ?>
</form>