<?php
session_start();
$errors = [];
require_once 'block/header.php';
require_once 'utils/function.php';
require_once  'utils/bdd_connexion.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    $errors = login($pdo, $_POST['login'], $_POST['password']);

    if (count($errors) == 0){
        header('location: adminpage.php');
    }
}
    ?>
<link rel="stylesheet" href="assets/style/login.css">
<div class="wrapper fadeInDown">
    <div id="formContent"
    <!-- Tabs Titles -->
    <!-- Login Form -->
    <form method="post">
        <input type="email" id="username" class="fadeIn second" name="login" placeholder="Entrez votre adresse mail">
        <input type="password" id="password" class="fadeIn third" name="password" placeholder="Mot de passe">
        <input type="submit" class="fadeIn fourth" value="Se connecter">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
        <a class="underlineHover" href="">S'incrire</a><br>
        <a class="underlineHover" href="">Mot de passe oublier ?</a>
    </div>

    <?php
    if (count($errors)>0){
        echo ('<p>les erreurs : </p>');
        foreach ($errors as $error){
            echo ('<li>'.$error.'</li>');
        }
    }
    ?>
</div>