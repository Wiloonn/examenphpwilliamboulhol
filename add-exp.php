<?php
require_once 'utils/bdd_connexion.php';
require_once  'utils/function.php';

$errors = [];
if ( $_SERVER['REQUEST_METHOD'] === 'POST') {
    $returnValidation = validateFormExp();
    $errors = $returnValidation['errors'];
    if (count($errors) === 0) {
        addExp($pdo);
        header('Location: adminpage.php');
    }
}
?>

<form method="post" action="add-exp.php" enctype="multipart/form-data">
    <label>Titre de l'expérience</label>
    <input type="text" name="titre" class="form-control" placeholder="Titre de l'expérience">
    <label>Description de l'expérience</label>
    <input type="text" name="description" class="form-control" placeholder="Description de l'expérience">
    <label>Date de début de l'expérience</label>
    <input type="date" name="date_debut" class="form-control">
    <label>Date de fin de l'expérience</label>
    <input type="date" name="date_fin" class="form-control">

    <input type="submit">

    <?php
    if(count($errors) != 0){
        echo(' <h2>Erreurs lors de la dernière soumission du formulaire : </h2>');
        foreach ($errors as $error){
            echo('<div class="error">'.$error.'</div>');
        }
    }
    ?>
</form>