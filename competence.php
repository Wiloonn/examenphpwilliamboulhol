<?php
require_once 'block/header.php';
require_once 'utils/bdd_connexion.php';
?>
<h2>Compétences : </h2>
<?php
$reponse = $pdo->query('SELECT * FROM competence');
while ($data = $reponse->fetch())
{
    ?>
    <div>
        <h3><?php echo ($data['titre']); ?></h3>
        <p>Note : <?php echo ($data['note']); ?></p>
    </div>
    <?php
}
$reponse->closeCursor();
?>