<!doctype html>
<html lang="fr">
<head>
    <link rel="icon" type="image/png" href="" />
    <meta charset="utf-8">
    <title>Examen Php</title>
    <link rel="stylesheet" href="">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="competence.php">Compétences<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="experience.php">Expériences<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="login.php">Se connecter<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="disconnect.php">Se déconnecter<span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>

<h1>CV en ligne</h1><br>
<a href="add-competence.php">Ajouter une compétence</a>
<h2>Compétences :</h2>
<?php
require_once 'utils/bdd_connexion.php';

$reponse = $pdo->query('SELECT * FROM competence');
while ($data = $reponse->fetch())
{
    ?>
    <div>
        <h3><?php echo ($data['titre']); ?></h3>
        <p>Note : <?php echo ($data['note']); ?></p>
    </div>

    <a title="Editer" href="edit-competence.php?id=<?php echo($data['id']); ?>">Editer</a>
    <a title="Supprimer" href="delete-competence.php?id=<?php echo($data['id']);?>">Supprimer</a>
    <?php
}
$reponse->closeCursor();
?>
<hr>
<br>
<a href="add-exp.php">Ajouter une expérience</a>
<h2>Expériences :</h2>
<?php
require_once 'utils/bdd_connexion.php';

$reponse = $pdo->query('SELECT * FROM experience');
while ($data = $reponse->fetch())
{
    ?>
    <div>
        <h3><?php echo ($data['titre']); ?></h3>
        <p> <?php echo ($data['date_debut']);?> à <?php echo ($data['date_fin']);?><br>
            <?php echo ($data['description']); ?>
        </p>

    </div>

    <a title="Editer" href="edit-exp.php?id=<?php echo($data['id']); ?>">Editer</a>
    <a title="Supprimer" href="delete-exp.php?id=<?php echo($data['id']);?>">Supprimer</a>
    <?php
}
$reponse->closeCursor();
?>
</body>