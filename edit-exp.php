<?php

require_once 'utils/bdd_connexion.php';
require_once 'utils/function.php';

$idExp = $_GET['id'];
$experience = getExp($pdo, $idExp);
$errors = [];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $returnValidation = validateEditExp();
    $errors = $returnValidation['errors'];

    if (count($errors) === 0) {
        updateCompetence($pdo, $experience['id']);
        header('Location: adminpage.php');
    }
}

?>

<form method="post" action="edit-exp.php?id=<?php echo($experience['id']);?>" enctype="multipart/form-data">
    <label>Titre de la compétence</label>
    <input type="text" name="titre" class="form-control" placeholder="Titre de l'experience" value="<?php echo($experience['titre'])?>">
    <label>Description de l'expérience</label>
    <input type="text" name="description" class="form-control" placeholder="Description de l'expérience" value="<?php echo($experience['description'])?>">
    <label>Date de début de l'expérience</label>
    <input type="date" name="date_debut" class="form-control">
    <label>Date de fin de l'expérience</label>
    <input type="date" name="date_fin" class="form-control">
    <input type="submit">

</form>
