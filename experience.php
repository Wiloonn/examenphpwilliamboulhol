<?php
require_once 'block/header.php';
require_once 'utils/bdd_connexion.php';
?>
    <h2>Expériences : </h2>
<?php
$reponse = $pdo->query('SELECT * FROM experience');
while ($data = $reponse->fetch())
{
    ?>
    <div>
        <h3><?php echo ($data['titre']); ?></h3>
        <p> <?php echo ($data['date_debut']);?> à <?php echo ($data['date_fin']);?><br>
            <?php echo ($data['description']); ?>
        </p>

    </div>

    <?php
}
$reponse->closeCursor();
?>